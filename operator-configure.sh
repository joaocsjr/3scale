
#get 3scale admin password
oc get secret system-seed -o json | jq -r .data.ADMIN_USER | base64 -d
oc get secret system-seed -o json | jq -r .data.ADMIN_PASSWORD | base64 -d

#master password
oc get secret system-seed -o json | jq -r .data.MASTER_USER | base64 -d
oc get secret system-seed -o json | jq -r .data.MASTER_PASSWORD | base64 -d





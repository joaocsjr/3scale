# DO240

Class: 3scale
Created: September 27, 2022 10:12 AM
Materials: https://rol.redhat.com/rol/app/courses/do240-2.11
Reviewed: No
Type: Trainning

# Introdução 3scale

3scale api management é uma plataforma de gerenciamento de api, pode ser instalado on-premises, em cloud, como serviço gerenciado ou a combinação dessas opções

## Features

- Existem dois componentes lógicos diferentes: o API Manager e o API Gateway. Se o API Manager estiver inativo, o API Gateway ainda funcionará.
- Políticas de roteamento, limitação de taxa, gerenciamento de usuários e monetização.
- Os administradores podem gerenciar suas APIs por meio de uma interface de usuário GUI, o Admin Portal.
- O administrador pode gerenciar suas APIs por meio da linha de comando, o 3scale Toolbox CLI.

## Arquitetura do produto

![Untitled](DO240%200def038a32e646539fef69e0462110cc/Untitled.png)

- O API Gateway busca as informações do API Manager (políticas de roteamento, usuários, limites de taxa…) periodicamente e as salva, Isso significa que o API Gateway não precisa obter as informações do API Manager em todas as solicitações recebidas, reduzindo assim a latência.
- Ao mesmo tempo, se o API Manager estiver inativo por alguns minutos, o API Gateway poderá funcionar de forma independente, pois contém as informações armazenadas em cache.

## API Manager

É responsavel por gerenciar a toda a parte administrativa do 3scale, provendo o admin portal para gerenciamento das apis, ,routing policies, users, rate limits, etc…

> Routing Polices
> 

Você pode usar uma política de roteamento para definir uma rota externa onde os consumidores de suas APIs farão solicitações. Então, depois que essas solicitações chegarem ao 3scale, você define onde elas serão entregues.

Por exemplo, considere que você tem uma API de loja de livros disponível em [http://books.mystore.com/](http://books.mystore.com/). Você deseja que seus clientes consumam sua API por meio do 3scale para poder gerenciar diferentes aspectos da API. Considere também que você defina uma rota 3scale /books, que é a rota externa pública que seus clientes usarão. Com os recursos de roteamento do 3scale, você pode mapear o /books  roteia para seu serviço externo [http://books.mystore.com/](http://books.mystore.com/). O diagrama a seguir ilustra esse processo.

![Untitled](DO240%200def038a32e646539fef69e0462110cc/Untitled%201.png)

Para criar políticas de roteamento, o 3scale oferece duas abstrações: products e backends. Um products é usado para definir as rotas externas acessíveis por seus clientes.

Um back-end é usado para definir seus serviços de API. Em seguida, um produto é mapeado para um ou vários back-ends para atingir a política de roteamento. O diagrama a seguir ilustra o exemplo de livros com uma nova rota, /authors A rota /books é mapeada para [http://books.mystore.com/](http://books.mystore.com/) e a rota /authors é mapeada para [http://authors.mystore.com/](http://authors.mystore.com/).

![Untitled](DO240%200def038a32e646539fef69e0462110cc/Untitled%202.png)

Rate limits

Developer Portal

![Untitled](DO240%200def038a32e646539fef69e0462110cc/Untitled%203.png)

O Developer Portal é uma página da web pública que você pode usar para fornecer informações sobre suas APIs. O Portal do Desenvolvedor é totalmente personalizável e fornece recursos como importação de definições do Swagger, autenticação para desenvolvedores ou modelos de e-mail.

# Instalação 3scale

<aside>
💡 Register Redhat account `[https://access.redhat.com/terms-based-registry/](https://access.redhat.com/terms-based-registry/)`

</aside>

```bash

#create secret for resgitry authentication 

[user@host ~]$ oc create secret docker-registry \
  threescale-registry-auth \
  --docker-server=registry.redhat.io \
  --docker-username='6030602|3scale-lab'\ 
  --docker-password=eyJhbGciOiJSUzUxMiJ9.eyJzdWIiOiI2NTc1NmRmMjU2OTE0YTgzOGRjNDY3YWU4ZmM5YzFmYiJ9.CpcxPe1xXf5csEtXEc8j2S7kbcUGOXcNiUT3zfOY-Vaiuyw2mWYASAlv4EE2FtM0l58FJ3jDv64A-jsriZGA04pfYlX0_yaHpybwdgaEjKzAkoMaowzWZemLGMuShXRtE4uNpOq1kmshsXGoiiLXpKLVfhZldnNU1NYnIT92-nTLalcRv5OnWVdiUyAoSihSfnPDlFV_ibzNY2msVeWpUrHQmykcSR3b0D8rx-pP0XYvGQW2VAB5DN-U5MRbp611WaV20UgQeKzuvpGC9wvbIoLL_dAiozUa7aBHmF1JcOyUo0XYHGnDT94CwBlUOL2LaTvLOvtQQxolsvOy92Ewk2xE8YzF5QLE8J93LvwxRbnXroV6uaItaljypJjHl4dVIpo1phLqAfkUesy9uexi3y2zqkpXra1NkH9pcIRSdn9vMNZmnjQAyMoJjy7E4O_VdnjuIFTwEXp93iRC-7PkKE5TZQNniV8Ul9GIDimesUgq15sM4jN7gFfFEmIN5oaiOE94_AF5auZFast9CPe2ObIMcTa77WdP8ZOJgm520-Mzli96ILbTp74P2kiLjAs5SpaaYO0lPR8AeJekJ-xeF2ru3SSKVbWO9nL4dt61PUkOWL_i96Y59JgEvxZPMPuPLaC6Rpwj08vI1VPR2VHir4AyaRT-MSMOrNRMbaALT_I

[user@host ~]$ oc secrets link default \
 threescale-registry-auth --for=pull

[user@host ~]$ oc secrets link builder threescale-registry-auth

#install operator from webconsole 
 

console-openshift-console.apps.sno.mandrake.lab

```

```bash
#api manager deploy

apiVersion: apps.3scale.net/v1alpha1
kind: APIManager
metadata:
  name: apimanager-sample
  namespace: 3scale
spec:
  system:
    appSpec:
      replicas: 1
    sidekiqSpec:
      replicas: 1
  zync:
    appSpec:
      replicas: 1
    queSpec:
      replicas: 1
  backend:
    cronSpec:
      replicas: 1
    listenerSpec:
      replicas: 1
    workerSpec:
      replicas: 1
  apicast:
    productionSpec:
      replicas: 1
    stagingSpec:
      replicas: 1

  wildcardDomain: apps.sno.mandrake.lab
```

```bash

#get 3scale admin password

oc get secret system-seed -o json | jq -r .data.ADMIN_USER | base64 -d
oc get secret system-seed -o json | jq -r .data.ADMIN_PASSWORD | base64 -d

#master password
oc get secret system-seed -o json | jq -r .data.MASTER_USER | base64 -d
oc get secret system-seed -o json | jq -r .data.MASTER_PASSWORD | base64 -d

#get admin
oc get secret system-seed -n 3scale \
--template={{.data.ADMIN_PASSWORD}} | base64 -d; echo

\
```

[https://api-3scale-apicast-staging.apps.sno.mandrake.lab/user_key=0acb50444809ef46de829d24e9ae3bc5](https://api-3scale-apicast-staging.apps.sno.mandrake.lab/user_key=0acb50444809ef46de829d24e9ae3bc5)

### Toolbox

```bash

#configure docker registry

[user@host ~]$ podman login registry.redhat.io
Username: redhat_registry_username
Password: redhat_registry_token

alias 3scale="podman run \
registry.redhat.io/3scale-amp2/toolbox-rhel8:3scale2.11 3scale"

podman run --name=toolbox-original \
registry.redhat.io/3scale-amp2/toolbox-rhel8:3scale2.11 3scale remote \
add 3scale-tenant -k https://82f7b26487c711ba2552c0b04c049de8@3scale-admin.apps.sno.mandrake.lab

82f7b26487c711ba2552c0b04c049de8

# criando service equivalente ao product 

[user@host ~]$ 3scale service create \ 
3scale-tenant \ 
library-product3

```

### Gitlab runner

```bash
# register gitlab-runner 
$ kubectl -n operators create secret generic gitlab-secret --from-literal runner-registration-token=GR1348941cP7xdw5hGaxh1xmpq8Bn

```